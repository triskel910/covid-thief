﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Guard : MonoBehaviour
{
    public enum guardClass { Simple, fool }
    public guardClass type;

    private bool scout = true;
    private bool chase = false;
    private bool heard = false;
    private bool seen = false;
    private bool getLastPos = false;

    public NavMeshAgent nav;

    private int randomNumber;
    private int randomWait;
    public Vector3[] waypoints;
    [HideInInspector]
    public float speed, vision, hear;
    private float timer;
   

    private GameObject player;
    private Vector3 playerPos;
    private Vector3 LplayerPos;

    public float maxAngle;
    public float maxRadius;
    private bool isInFov = false;


    // Start is called before the first frame update
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        switch (type)
        {
            case guardClass.Simple:
                speed = 3.0f;
                vision = 20.0f;
                hear = 25.0f;
                break;
        }
        nav.speed = speed;
        GetRandomNumWaypoint();
        GetRandomNumWait();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        playerPos = player.transform.position;
        States();
        //  isInFov = inFOV(transform, player, maxAngle, maxRadius);
        CanSeePlayer();

       


    }
    void States()
    {
        if (scout)
        {

            nav.SetDestination(waypoints[randomNumber]);
            //when it stops
            if (nav.remainingDistance <= nav.stoppingDistance)
            {
                timer += Time.fixedDeltaTime;
                if (timer >= randomWait)
                {
                    GetRandomNumWaypoint();
                    timer = 0.0f;
                }
                GetRandomNumWait();
            }

        }
        if (chase)
        {
            nav.SetDestination(playerPos);
        }

        if (getLastPos)
        {
            getLastPlayerPos();
            getLastPos = false;
        }

    }


    void GetRandomNumWaypoint()
    {
        randomNumber = Random.Range(0, 7);

    }
    void GetRandomNumWait()
    {
        randomWait = Random.Range(5, 15);

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, maxRadius);

        Vector3 fovLine1 = Quaternion.AngleAxis(maxAngle, transform.up) * transform.forward * maxRadius;
        Vector3 fovLine2 = Quaternion.AngleAxis(-maxAngle, transform.up) * transform.forward * maxRadius;

        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, fovLine1);
        Gizmos.DrawRay(transform.position, fovLine2);

        if (!isInFov)
            Gizmos.color = Color.red;
        else
            Gizmos.color = Color.green;


        Gizmos.color = Color.black;
        Gizmos.DrawRay(transform.position, transform.forward * maxRadius);


    }



    protected bool CanSeePlayer()
    {
        RaycastHit hit;
        Vector3 rayDirection = player.transform.position - transform.position;

        if ((Vector3.Angle(rayDirection, transform.forward)) <= maxAngle * 0.5f)
        {
            // Detect if player is within the field of view
            if (Physics.Raycast(transform.position, rayDirection, out hit, maxRadius))
            {

                scout = false;
                seen = true;
                chase = true;
                return (hit.transform.CompareTag("Player"));
            }
            else
            {

            }
        }

        return false;
    }
    void getLastPlayerPos()
    {
        LplayerPos = player.transform.position;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            
            if (seen)
            {
                getLastPos = true;
                chase = false;
                nav.SetDestination(LplayerPos);
                StartCoroutine(CheckPos(4f));
               
            }
        }
    }
    private IEnumerator CheckPos(float value)
    {
        yield return new WaitForSeconds(value);
        scout = true;
        seen = false;
    }

}
