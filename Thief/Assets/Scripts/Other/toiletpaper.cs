﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toiletpaper : MonoBehaviour
{
    private Vector3 start;
    
    // Start is called before the first frame update
    void Start()
    {
        start = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, 0.8f + Mathf.PingPong(Time.time * 0.36f, 0.5f), transform.position.z);
        transform.Rotate(Time.time * 0.36f, 0,0);
    }
}
